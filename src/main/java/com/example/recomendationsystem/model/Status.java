package com.example.recomendationsystem.model;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
