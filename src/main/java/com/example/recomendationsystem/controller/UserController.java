package com.example.recomendationsystem.controller;

import com.example.recomendationsystem.dto.RegistrationRequest;
import com.example.recomendationsystem.dto.UserDto;
import com.example.recomendationsystem.model.User;
import com.example.recomendationsystem.service.UserService;
//import io.swagger.v3.oas.annotations.Operation;
//import io.swagger.v3.oas.annotations.media.Content;
//import io.swagger.v3.oas.annotations.media.Schema;
//import io.swagger.v3.oas.annotations.responses.ApiResponse;
//import io.swagger.v3.oas.annotations.responses.ApiResponses;
//import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/api/register")
    public ResponseEntity<String> register(@RequestBody RegistrationRequest request) {
        try {
            userService.registration(request);
            return ResponseEntity.ok("User registered successfully.");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred during registration.");
        }
    }

//    @Operation(
//            summary = "Retrieve a Tutorial by Id",
//            description = "Get a Tutorial object by specifying its id. The response is Tutorial object with id, title, description and published status.",
//            tags = { "tutorials", "get" })
//    @ApiResponses({
//            @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = Tutorial.class), mediaType = "application/json") }),
//            @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema()) }),
//            @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema()) }) })
//    @GetMapping("/tutorials/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable(name = "id") Long id){
        User user = userService.findById(id);

        if(user == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        UserDto result = UserDto.fromUser(user);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/username/{username}")
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        User user = userService.findByUsername(username);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getAll();
        return ResponseEntity.ok(users);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }

//    @ApiResponses({
//            @ApiResponse(responseCode = "200", content = { @Content(mediaType = "application/json",
//                    schema = @Schema(implementation = Employee.class)) }),
//            @ApiResponse(responseCode = "404", description = "Employee not found",
//                    content = @Content) })
//    @DeleteMapping("/employees/{employeeId}")
//    public String deleteEmployee(@PathVariable int employeeId) {
//        Employee employee = repository.findById(employeeId)
//                .orElseThrow(() -> new RuntimeException("Employee id not found - " + employeeId));
//        repository.delete(employee);
//        return "Deleted employee with id: " + employeeId;
//    }

//    public Employee getEmployee(@Parameter(
//            description = "ID of employee to be retrieved",
//            required = true)
//                                @PathVariable int employeeId) {
//        Employee employee = repository.findById(employeeId)
//                .orElseThrow(() -> new RuntimeException("Employee id not found - " + employeeId));
//        return employee;

}