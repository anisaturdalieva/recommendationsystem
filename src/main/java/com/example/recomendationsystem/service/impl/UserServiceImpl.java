package com.example.recomendationsystem.service.impl;

import com.example.recomendationsystem.dto.AuthenticationRequestDto;
import com.example.recomendationsystem.dto.AuthenticationResponseDto;
import com.example.recomendationsystem.dto.RegistrationRequest;
import com.example.recomendationsystem.model.Role;
import com.example.recomendationsystem.model.Status;
import com.example.recomendationsystem.model.User;
import com.example.recomendationsystem.repository.RoleRepository;
import com.example.recomendationsystem.repository.UserRepository;
import com.example.recomendationsystem.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public User registration(RegistrationRequest request) {
        if (!isValidEmail(request.getEmail())) {
            throw new IllegalArgumentException("Email must be valid.");
        }

        if (request.getPassword().length() < 8) {
            throw new IllegalArgumentException("Password length must be at least 8 characters.");
        }

        if (!Objects.equals(request.getPassword(), request.getRepeatPassword())) {
            throw new IllegalArgumentException("Password and repeat password must match.");
        }

        if (!request.isTermsAndConditions()) {
            throw new IllegalArgumentException("You must agree to the terms and conditions.");
        }

        User user = new User();
        user.setEmail(request.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        user.setStatus(Status.ACTIVE);

        Role userRole = roleRepository.findByName("ROLE_USER");
        if (userRole == null) {
            throw new RuntimeException("Default user role not found.");
        }
        user.setRoles( Collections.singletonList(userRole));

        User registeredUser;
        try {
            registeredUser = userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException("Email already exists.");
        }

        log.info("IN register - request: {} successfully registered", registeredUser);

        return registeredUser;
    }
    private boolean isValidEmail(String email) {
        return email.matches("^[\\w.-]+@[\\w.-]+\\.[a-z]{2,}$");
    }


    @Override
    public AuthenticationResponseDto login(AuthenticationRequestDto requestDto) {
        return null;
    }

    @Override
    public User findById(Long id) {
        User result = userRepository.findById ( id ).orElse ( null );

        if (result == null){
            log.warn ( "IN findById - no user found by id: {}", id );
            return null;
        }
        log.info ( "IN findById - user: {} found by userId: {} ", result, id );
        return result;
    }

    @Override
    public User findByUsername(String username) {
        User result = userRepository.findByUsername(username);
        log.info("IN findByUsername - user: {} found by username: {}", result, username);
        return result;
    }

    @Override
    public List<User> getAll() {
        List<User>result = userRepository.findAll (  );
        log.info ( "IN getAll - {} users found", result.size () );
        return result;
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById ( id );
        log.info ( "IN delete - user with id: {} successfully delete ", id );
    }
}
