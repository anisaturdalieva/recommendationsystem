package com.example.recomendationsystem.service;

import com.example.recomendationsystem.dto.AuthenticationRequestDto;
import com.example.recomendationsystem.dto.AuthenticationResponseDto;
import com.example.recomendationsystem.dto.RegistrationRequest;
import com.example.recomendationsystem.model.User;

import java.util.List;

public interface UserService {

    User registration(RegistrationRequest request);
    AuthenticationResponseDto login (AuthenticationRequestDto requestDto);
    User findById(Long id);
    User findByUsername(String userName);
    List<User> getAll();
    void delete(Long id);
}