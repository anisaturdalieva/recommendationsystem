package com.example.recomendationsystem.repository;

import com.example.recomendationsystem.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String name);

    boolean existsByEmail(String email);
}