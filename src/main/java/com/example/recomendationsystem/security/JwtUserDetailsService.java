package com.example.recomendationsystem.security;

import com.example.recomendationsystem.model.User;
import com.example.recomendationsystem.security.jwt.JwtUser;
import com.example.recomendationsystem.security.jwt.JwtUserFactory;
import com.example.recomendationsystem.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class JwtUserDetailsService  implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername ( username );

        if (user == null){
            throw new UsernameNotFoundException ( "User with userName: " + username + "not found" );
        }
        JwtUser jwtUser = JwtUserFactory.create ( user );
        log.info ( "IN loadUserByUserName - user with userName: {} successfully loaded", username);
        return jwtUser;
    }
}
